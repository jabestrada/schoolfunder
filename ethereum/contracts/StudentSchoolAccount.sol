pragma solidity ^0.4.25;

contract StudentSchoolAccountFactory {
    address[] public deployedAccounts;
   
    function getDeployedAccounts() public view returns (address[]) {
        return deployedAccounts;
    }

    function createStudentSchoolAccount(address student, address school) public {
        address newAccount = new StudentSchoolAccount(student, school);
         deployedAccounts.push(newAccount);
    }    

}

contract StudentSchoolAccount {
    address public donee;
    address public school;
    
    
    constructor(address _donee, address _school) public {
        donee = _donee;
        school = _school;
    }
    
    function getBalance() public view returns (uint) {
        return address(this).balance;
    }

    event Log(string message);
    
    event FundsReceived(uint fundIndexId, address donee, uint amount, 
                        uint updatedAccountBalance);
                        
    event PaymentMade(string reference, address paidBy, uint amountPaid, 
                        uint payablebalancePostPayment,
                        uint accountBalancePostPayment);
    
    // START: Payable
    enum PayableStatus { Open, FullyPaid, Deleted }
    struct Payable {
        uint amountDue;
        uint balanceDue;
        string forWhat;
        uint dueDate;
        PayableStatus status;
        uint payableIndexKey;
    }
    mapping(string => Payable) payables;
    string[] payablesIndex;
    // END: Payable
    
    // START: Fund
    enum FundStatus {Uninitialized, Ok, Deleted }
    struct Fund {
        uint amount;
        address donor;
        uint receivedOn;
        uint balance; 
        FundStatus status;
        uint fundIndexKey;
    }
    mapping(string => Fund) funds;
    uint[] fundsIndex;
    // END:
    
    // START: Payments
    // Payments are strictly read-only for audit purposes, 
    // so no need for mapping/random access. 
    struct Payment {
        uint amount;
        uint paidOn;
        string payableRef;
    }
    Payment[] paymentsIndex;
    // END: Payments
    
    // START: public API; should be controllable by kill switch.
    /*
    function () public payable killSwitchDisabled { 
        
    }
    */
    
    // Fund key is primary key in funds mapping;
    // should be unique and client-generated.
    function sendFunds(string fundKey) public payable 
            fundKeyUnique(fundKey) 
            uintGreaterThan(msg.value, 0, "Sent funds should be greater than zero")
            killSwitchDisabled {
        
        uint sentAmount = msg.value;
        
        // Contemplated on incentivizing school with a "small" fee of 
        // each fund transfer; decided that it's better settled off-chain.
        
        uint newFundIndex = createFund(fundKey, sentAmount, msg.sender);
        
        emit FundsReceived(newFundIndex, msg.sender, sentAmount, address(this).balance);
        // Decided to move auto-settling of outstanding payables to client logic
    }
    // END: public API
    
    // START: Non-public API
    function createFund(string key, uint amount, address donor) 
        private
        fundKeyUnique(key) 
         returns (uint) {
        uint newFundIndex = fundsIndex.push(fundsIndex.length);
        funds[key].fundIndexKey = newFundIndex - 1;
        funds[key].amount = amount * 1 ether;
        funds[key].balance = amount * 1 ether;
        funds[key].donor = donor;
        funds[key].receivedOn = block.timestamp;
        funds[key].status = FundStatus.Ok;
        return newFundIndex;
    }
    // END: Non-public API
    
    // START: System config; must be private and have restricted getters/setters
    bool private killSwitchOn = false;
    function getKillSwitch() public view 
    restricted returns (bool) {
        return killSwitchOn;
    }
    
    function setKillSwitch(bool newSetting) public restricted {
        killSwitchOn = newSetting;
    }
    
    modifier killSwitchDisabled {
        require(killSwitchOn == false, "System is under maintenance");
        _;
    }
    
    modifier isValidDate(uint dateValue, string message) {
        // TODO: Implement this...
        _;
    }
    
    modifier restricted {
        require(msg.sender == school, "Access restricted to school account");
        _;
    }
    
    modifier payableRefUnique(string payableRef){
        require(payables[payableRef].amountDue == 0, "Payable reference already exists");
        _;    
    }
    
    modifier uintGreaterThan(uint value1, uint value2, string failMessage) {
        require(value1 > value2, failMessage);
        _;
    }
    
    modifier fundKeyUnique(string key){
        require(funds[key].status == FundStatus.Uninitialized, "FundKey already exists");
        _;
    }
    // END: System config
    
 

    // START: restricted API
    /*
        TODO:
        1. CRUD for Payable items; UD only allowed for payables where balance == amount (i.e., no payments drawn to-date against payable item).
        2. Prune/archive funds[] where status IN (FullyPaid, Deleted)
        3. Retrieve payments for off-chain archiving/reporting; 
           include start/end indices in parameter list for query pagination.
    */

    // payableRef should be unique and is usually an invoice.
    function createPayable(string payableRef, uint amountDue, 
                string forWhat, uint dueDate)
                public
                uintGreaterThan(amountDue, 0, "amountDue should be greater than zero")
                isValidDate(dueDate, "dueDate is not a valid Date value")
                payableRefUnique(payableRef)
                restricted {
        
        uint newIndex = payablesIndex.push(payableRef);
        payables[payableRef].payableIndexKey = newIndex - 1;
        payables[payableRef].amountDue = amountDue * 1 ether;
        payables[payableRef].balanceDue = amountDue * 1 ether;
        payables[payableRef].forWhat = forWhat;
        payables[payableRef].dueDate = dueDate;
        payables[payableRef].status = PayableStatus.Open;
    }
    
    function computePayables() public view returns (uint) {
        uint payableBalance = 0;
        for (uint k = 0; k < payablesIndex.length; k++) {
            Payable storage payableItem = payables[payablesIndex[k]];
            if (payableItem.status == PayableStatus.Open && payableItem.balanceDue > 0) {
                payableBalance = payableBalance + payableItem.balanceDue;
            }
        }
        return payableBalance;
    }
    
    function settlePayables() public restricted {
        if (address(this).balance == 0) {
            emit Log("settlePayables called but account has zero balance");
            return;
        }
        
        bool hasPayable = false;
        for (uint k = 0; k < payablesIndex.length; k++){
            uint amountPaid = 0;
            Payable storage payableItem = payables[payablesIndex[k]]; 
            // We'll have archiving (manually triggered) of non-Open payables 
            // but be defensive just the same.
            if (payableItem.status != PayableStatus.Open) continue;
            
            hasPayable = true;
            
            uint accountBalance = address(this).balance;
            if (payableItem.balanceDue > accountBalance) {
                amountPaid = accountBalance;
                payableItem.balanceDue = payableItem.balanceDue - accountBalance;
            } else {
                // Balance is sufficient to fully pay off this payable.
                amountPaid = payableItem.balanceDue;
                payableItem.balanceDue = 0;
                payableItem.status = PayableStatus.FullyPaid;
            }
            // TODO: Convert impl below to Withdrawal pattern???
            address(school).transfer(amountPaid);
            
            emit PaymentMade(payablesIndex[payableItem.payableIndexKey], 
                            msg.sender, amountPaid, 
                            payableItem.balanceDue, address(this).balance);
            
            if (address(this).balance == 0) break;
        }
        
        if (!hasPayable) {
            emit Log("settlePayables called but no outstanding payables for the account");
        }
    }
    // END: restricted API
}