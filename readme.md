
# SchoolFunder Solidity Contract and Tests

## To run tests:

1. git clone https://jabestrada@bitbucket.org/jabestrada/schoolfunder.git

2. cd schoolfunder

3. npm install

4. npm run test

## To regenerate /build folder and contained contract files (required after modifying /contracts/*.sol files)
1. cd ethereum
2. node compile.js
