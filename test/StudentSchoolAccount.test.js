const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());

const compiledFactory = require('../ethereum/build/StudentSchoolAccountFactory.json');
const compiledStudentSchoolAccount = require('../ethereum/build/StudentSchoolAccount.json');


let factory;
let studentAddress;
let schoolAddress;

let studentSchoolAccount;
let studentSchoolAccountAddress;

let donorAccount1Address;

const defaultGas = '3000000';

function parseEtherFromWei(weiValue) {
    return parseFloat(web3.utils.fromWei(weiValue, 'ether'));
}


function etherToWei(etherValue){
    return web3.utils.toWei(etherValue, 'ether');
}

const getRandomKey = () => web3.utils.randomHex(4);


// ARRANGE (global)
beforeEach(async () => {
    
    [schoolAddress, studentAddress, donorAccount1Address] = await web3.eth.getAccounts();

    // Deploy factory
    factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
                .deploy({ data: compiledFactory.bytecode })
                .send({ from: schoolAddress, 
                    gas: defaultGas});
    
    await factory.methods.createStudentSchoolAccount(studentAddress, schoolAddress)
            .send( {
                from: schoolAddress,
                gas: defaultGas
    });
    

    [studentSchoolAccountAddress] = await factory.methods.getDeployedAccounts().call();

    studentSchoolAccount = await new web3.eth.Contract(
        JSON.parse(compiledStudentSchoolAccount.interface),
        studentSchoolAccountAddress
    );
});


// ACT and ASSERT
describe('StudentSchoolAccount', () => {
    /*
        TODO test cases:
        - restricts killSwitch configuration to school account
        - processes full payments after which it should produce 
            - correct account balance 
            - correct payable item balance(s) 
        - processes partial payments after which it should produce 
            - correct account balance 
            - correct payable item balance(s) 
        - validates sendFunds() against <= zero amounts
        - validates createPayable against <= zero amounts
        - validates createPayable against invalidate dueDate
    */

    it('deploys a factory and student school account', () => {
        assert.ok(factory.options.address);
        assert.ok(studentSchoolAccount.options.address);
    });

    it('can accept funds', async () => {
        const oldBalance = parseEtherFromWei(await web3.eth.getBalance(studentSchoolAccount.options.address));    
        const donation =  etherToWei('1');
        const expectedBalance = oldBalance + parseEtherFromWei(donation);        
        await studentSchoolAccount.methods
                .sendFunds(getRandomKey())
                .send({from: donorAccount1Address, 
                    gas: defaultGas,
                    value: donation });    
        const newBalance = parseEtherFromWei(await web3.eth.getBalance(studentSchoolAccount.options.address));
    
        assert.equal(newBalance, expectedBalance);
    });
    
    it('cannot accept funds if kill switch is enabled', async () => {   
        await studentSchoolAccount.methods
                .setKillSwitch(true)
                .send({
                    from: schoolAddress,
                    gas: defaultGas
                }); 
        
        const oldBalance = parseEtherFromWei(await web3.eth.getBalance(studentSchoolAccount.options.address));    
        try {    
            const donation =  etherToWei('1');       
            await studentSchoolAccount.methods
                    .sendFunds(getRandomKey())
                    .send({from: donorAccount1Address, 
                        gas: defaultGas,
                        value: donation });    
            assert(false, "sendFunds() succeeded when it was expected to fail");
        }
        catch 
        {
            assert(true);
        }
        // Ensure that balance remained the same.
        const currentBalance = parseEtherFromWei(await web3.eth.getBalance(studentSchoolAccount.options.address));
        assert.equal(currentBalance, oldBalance);
    });


    it('rejects funds with duplicate fund key', async () => {
        const oldBalance = parseEtherFromWei(await web3.eth.getBalance(studentSchoolAccount.options.address));    
        const donation =  etherToWei('1');
        const expectedBalance = oldBalance + parseEtherFromWei(donation);
        const dummyKey = getRandomKey();
        await studentSchoolAccount.methods
                .sendFunds(dummyKey)
                .send({from: donorAccount1Address, 
                    gas: defaultGas,
                    value: donation });    
        let newBalance = parseEtherFromWei(await web3.eth.getBalance(studentSchoolAccount.options.address));
        assert.equal(newBalance, expectedBalance);

        // Attempt with same fund key; should fail.
        try {
            await studentSchoolAccount.methods
            .sendFunds(dummyKey)
            .send({from: donorAccount1Address, 
                gas: defaultGas,
                value: donation });    
            assert(false);
        }
        catch(err) {
            assert(true);
        }

        // Assert that failed sendFunds() didn't cause balance to change.
        newBalance = parseEtherFromWei(await web3.eth.getBalance(studentSchoolAccount.options.address));

        assert.equal(newBalance, expectedBalance);
    });

    it('can create payables', async () => {
        const oldPayables = parseEtherFromWei(await studentSchoolAccount.methods
                            .computePayables().call());

        let newPayableAmount = etherToWei('2');
        await studentSchoolAccount.methods
                .createPayable('Invoice#1',
                                newPayableAmount,
                                'Lab fees',
                                new Date().getTime())
                .send({ from: schoolAddress, 
                        gas: defaultGas});   

        const updatedPayables = parseEtherFromWei(await studentSchoolAccount.methods
                                .computePayables().call());
    
        assert.equal(oldPayables + newPayableAmount, updatedPayables);
    });


    it('rejects payables with duplicate key', async () => {
        const oldPayables = parseEtherFromWei(await studentSchoolAccount.methods
                            .computePayables().call());
        const newPayableAmount = etherToWei('2');
        const expectedPayableAmount = oldPayables + newPayableAmount;
        const randomKey = getRandomKey();
        await studentSchoolAccount.methods
                .createPayable(randomKey,
                                newPayableAmount,
                                'Lab fees',
                                new Date().getTime())
                .send({ from: schoolAddress, 
                        gas: defaultGas});   

        let updatedPayables = parseEtherFromWei(await studentSchoolAccount.methods
                                .computePayables().call());
        assert.equal(expectedPayableAmount, updatedPayables);

        // Attempt with same payable item key; should fail.
        try {
            await studentSchoolAccount.methods
            .createPayable(randomKey,
                            newPayableAmount,
                            'Lab fees again',
                            new Date().getTime())
            .send({ from: schoolAddress, 
                    gas: defaultGas});   
            assert(false);
        }
        catch(err) {
            assert(true);
        }

        // Assert that failed createPayable() didn't cause payables to change.
        updatedPayables = parseEtherFromWei(await studentSchoolAccount.methods
            .computePayables().call());
        assert.equal(expectedPayableAmount, updatedPayables);

    });

    it('can create payables', async () => {
        const oldPayables = parseEtherFromWei(await studentSchoolAccount.methods
                            .computePayables().call());

        let newPayableAmount = etherToWei('2');
        await studentSchoolAccount.methods
                .createPayable(getRandomKey(),
                                newPayableAmount,
                                'Lab fees',
                                new Date().getTime())
                .send({ from: schoolAddress, 
                        gas: defaultGas});   

        const updatedPayables = parseEtherFromWei(await studentSchoolAccount.methods
                                .computePayables().call());
    
        assert.equal(oldPayables + newPayableAmount, updatedPayables);
    });


    it('rejects payables if created from non-school account', async () => {
        const oldPayables = parseEtherFromWei(await studentSchoolAccount.methods
                            .computePayables().call());
        let newPayableAmount = etherToWei('2');
        
        // Attempt with student account; should fail.
        try {
            await studentSchoolAccount.methods
            .createPayable(getRandomKey(),
                            newPayableAmount,
                            'Lab fees again',
                            new Date().getTime())
            .send({ from: studentAddress, 
                    gas: defaultGas});   
            assert(false, "createPayable from non-school account shouldn't have succeeded");
        }
        catch(err) {
            assert(true);
        }

        // Assert that failed createPayable() didn't cause payables to change.
        updatedPayables = parseEtherFromWei(await studentSchoolAccount.methods
            .computePayables().call());
        assert.equal(oldPayables, updatedPayables);

    });

});

